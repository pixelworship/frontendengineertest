﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FurnitureOrientation {
	VERTICAL = 0,
	HORIZONTAL = 1
}

public class Furniture : MonoBehaviour {
	public Sprite [] sprites;

	[Header("Smoke Particles")]
	public GameObject smokeParticlesGO;

	private FurnitureOrientation orientation;
	public FurnitureOrientation Orientation {
		get { return orientation; }
		set {
			orientation = value;
			spriteRenderer.sprite = sprites[(int)orientation];
		}
	}
	

	public Vector2Int tileSize = new Vector2Int(2, 1);
	private SpriteRenderer spriteRenderer;
	private Vector2Int tileOriginIndex = new Vector2Int(0, 0);
	public Vector2Int TileOriginIndex {
		get { return tileOriginIndex; }
		set {
			tileOriginIndex = value;
			// Sort table based on tile index. with 32k max sort order this really isn't suitable for large maps but works fine for this demonstration
			spriteRenderer.sortingOrder = System.Int16.MaxValue - tileOriginIndex.y * MapTile.tileSize - tileOriginIndex.x;
		}
	}
	void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void AnimateIn() {
		Vector3 finalPosition = gameObject.transform.position;
		// Animate in
		iTween.MoveFrom(gameObject, iTween.Hash("y", gameObject.transform.position.y + 5.0f, "time", 0.3f));

		StartCoroutine(CreateSmokeParticles(finalPosition));
	}

	IEnumerator CreateSmokeParticles(Vector3 origin) {
		yield return new WaitForSeconds(0.25f);
		GameObject smokeGO = GameObject.Instantiate(smokeParticlesGO, origin, Quaternion.identity);

		// FIXME: Ugly hack since we know tables are 2x1
		if (orientation == FurnitureOrientation.HORIZONTAL) {
			Vector3 additiveSmokeOrigin = new Vector3(-MapTile.worldTileSize, MapTile.worldTileSize * 0.5f, 0);
			GameObject smokeGO2 = GameObject.Instantiate(smokeParticlesGO, origin + additiveSmokeOrigin, Quaternion.identity);
		} else {
			Vector3 additiveSmokeOrigin = new Vector3(MapTile.worldTileSize, MapTile.worldTileSize * 0.5f, 0);
			GameObject smokeGO2 = GameObject.Instantiate(smokeParticlesGO, origin + additiveSmokeOrigin, Quaternion.identity);
		}
	}
}

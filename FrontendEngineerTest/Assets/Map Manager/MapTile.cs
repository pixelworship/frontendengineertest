﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MapTileType {
	DIRT = 0,
	GRASS = 1,
	STONE = 2,
	WOOD = 3
}

public class MapTile : MonoBehaviour {
	public static int tileSize = 16;
	public static float worldTileSize = tileSize / 100.0f; // 100.0f represent pixels per unit

	public Vector2Int index;

	private BoxCollider2D boxCollider;
	private bool highlight = false;
	public bool Highlight {
		get { return highlight; }
		set {
			highlight = value;
			if (highlight) {
				spriteRenderer.material.color = new Color(1, 0, 1, 1);
			} else {
				spriteRenderer.material.color = new Color(1, 1, 1, 1);
			}
		}
	}
	private SpriteRenderer spriteRenderer = null;

	// Factory method to create tile
	public static GameObject CreateTile(int x, int y, TileSetData tileSet, int tileSetIndex) {
		GameObject tileGO = new GameObject("tile_" + x + "_" + y);
		MapTile mapTileScript = tileGO.AddComponent<MapTile>();
		mapTileScript.index = new Vector2Int(x, y);

		GameObject spriteGO = new GameObject("Sprite");
		// spriteGO.layer = LayerMask.NameToLayer("TileMap");

		spriteGO.transform.SetParent(tileGO.transform);

		//Add sprite renderer to it
		mapTileScript.spriteRenderer = (SpriteRenderer)spriteGO.AddComponent(typeof(SpriteRenderer));
		//Set sprite
		mapTileScript.spriteRenderer.sprite = tileSet.sprites[tileSetIndex];
		mapTileScript.spriteRenderer.sortingLayerName = "Floor";

		tileGO.transform.position = new Vector2(-x * MapTile.worldTileSize, y * MapTile.worldTileSize);

		// Add box collider
		mapTileScript.boxCollider = tileGO.AddComponent<BoxCollider2D>();
		mapTileScript.boxCollider.size = new Vector2(worldTileSize, worldTileSize);
		mapTileScript.boxCollider.isTrigger = true;

		if (tileSetIndex == (int)MapTileType.WOOD) {
			tileGO.AddComponent<MapTileOccupiable>();
		}

		// Sort table based on tile index. with 32k max sort order this really isn't suitable for large maps but works fine for this demonstration
		mapTileScript.spriteRenderer.sortingOrder = System.Int16.MaxValue - y * MapTile.tileSize - x;

		return tileGO;
	}
}

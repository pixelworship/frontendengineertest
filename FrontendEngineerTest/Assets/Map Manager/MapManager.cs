﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Utilities;

public class MapTileJson {
	public int TileType;
}

// Following two JSON classes are used simply to parse data json via schema and never used again
public class MapJson {
	public List<List<MapTileJson>> TerrainGrid;
}

public class MapManager : MonoBehaviour {
	public Vector2 MapSize {
		get {
			return new Vector2Int(tiles.GetLength(0), tiles.GetLength(1));
		}
	}
	
	[Header("Map json file name (ie, data_alt.json)")]
	public string mapFileName = "data.json";
	public GameObject [,] tiles;
	public TileSetData tileSet;
	// Vertical skew factor (y-scale). 0.5f seems to match table angle perfectly.
	[Header("Vertical squash / skew - 0.5 matches angle of furniture art")]
	public float verticalSkewFactor = 0.5f;

	public GameObject tableFurniture;
	// Child of root. Contains all the tiles
	private GameObject tileContainerGO;
	void Start () {
		LoadMap(mapFileName);
	}

	bool LoadMap(string mapFileName) {
		// FIXME: Application.streamingAssesPath will be different path depending on platform:
		// https://docs.unity3d.com/Manual/StreamingAssets.html
		string filePath = Application.streamingAssetsPath + "/" + mapFileName;
		if (!File.Exists(filePath)) {
			Debug.LogError("Failed to load " + filePath);
			return false;
		}

		string jsonData = File.ReadAllText(filePath);

		if (jsonData == null) {
			Debug.LogError("Failed to load " + filePath);
			return false;
		}
		
		MapJson gridMap = JsonConvert.DeserializeObject<MapJson>(jsonData);

		if (gridMap.TerrainGrid.Count == 0) {
			Debug.LogError("Failed to load " + filePath);
			return false;
		}

		// FIXME: Assuming all rows contain same number of tiles
		tiles = new GameObject[gridMap.TerrainGrid.Count, gridMap.TerrainGrid[0].Count];
		tileContainerGO = new GameObject("Map Container");

		for (int y = 0; y < gridMap.TerrainGrid.Count; y++) {
			List<MapTileJson> mapRow = gridMap.TerrainGrid[y];
			for (int x = 0; x < mapRow.Count; x++) {
				// Create tile
				GameObject tile = MapTile.CreateTile(x, y, tileSet, mapRow[x].TileType);
				tile.transform.SetParent(tileContainerGO.transform);

				tiles[x, y] = tile;
			}
		}

		// Using euler is fine in this case since there is only one rotation - no chance for gimble lock - it's a 2d demo!
		tileContainerGO.transform.localEulerAngles = new Vector3(0, 0, -45);
		tileContainerGO.transform.SetParent(gameObject.transform);
		gameObject.transform.localScale = new Vector3(1, verticalSkewFactor, 1);

		AnimateTilesIn();

		return true;
	}

	// FIXME: Below is naive solution simply to complete objective (fixed 2x1 furniture)
	// To scale to multiple furniture sizes, below would needed to be modified slightly to accomodate
	public void PlaceFurnitureIfPossible(Vector2Int tileIndex) {
		// Make sure tile has TileOccupiable script attached and is not occupied
		GameObject tileGO = tiles[(int)tileIndex.x, (int)tileIndex.y];
		if (!tileGO) {
			return;
		}

		MapTileOccupiable tileOccupiable = tileGO.GetComponent<MapTileOccupiable>();
		if (!tileOccupiable) {
			return;
		}

		if (tileOccupiable.isOccupied) {
			return;
		}
		
		// FIXME: Hack for 2x1 furniture only
		// Make sure there is at least one "open" tile attached to it
		// Order of these define placement priority (right, left, above, below)
		Vector2Int [] checkTileIndexes = new Vector2Int [] {
			new Vector2Int(1, 0),
			new Vector2Int(-1, 0),
			new Vector2Int(0, 1),
			new Vector2Int(0, -1)
		};

		for (int i = 0; i < checkTileIndexes.Length; i++) {
			// Make sure selected tile isn't a border tile
			Vector2Int indexOffset = checkTileIndexes[i];
			if (!TileIndexInRange(tileIndex + indexOffset)) {
				continue;
			}

			GameObject offsetTileGO = tiles[tileIndex.x + indexOffset.x, tileIndex.y + indexOffset.y];

			if (!offsetTileGO) {
				continue;
			}

			MapTileOccupiable offsetTileOccupiable = offsetTileGO.GetComponent<MapTileOccupiable>();
			if (!offsetTileOccupiable || offsetTileOccupiable.isOccupied) {
				continue;
			}

			tileOccupiable.isOccupied = true;
			offsetTileOccupiable.isOccupied = true;

			// FIXME: Highlighting used for debugging only
			// tileGO.GetComponent<MapTile>().Highlight = true;
			// offsetTileGO.GetComponent<MapTile>().Highlight = true;

			// Place furniture
			PlaceTable(tileIndex, tileIndex + indexOffset);
			return;
		}
	}

	// FIXME: PLace and orient table furniture
	// Again, just enough information to place 2x1 table - not yet scalable
	private void PlaceTable(Vector2Int index1, Vector2Int index2) {
		GameObject tile1GO = tiles[index1.x, index1.y];
		GameObject tile2GO = tiles[index2.x, index2.y];

		GameObject tableGO = GameObject.Instantiate(tableFurniture);
		Furniture furniture = tableGO.GetComponent<Furniture>();

		// Which tile should we use for origin?
		GameObject originTile = null;
		if (tile1GO.transform.position.x > tile2GO.transform.position.x) {
			originTile = index1.y == index2.y ? tile1GO : tile2GO;
		} else {
			originTile = index1.y == index2.y ? tile2GO : tile1GO;
		}

		// Orient properly
		furniture.Orientation = index1.y == index2.y ? FurnitureOrientation.HORIZONTAL : FurnitureOrientation.VERTICAL;

		// Position furniture and add to our map container go
		tableGO.transform.position = originTile.transform.position;
		furniture.TileOriginIndex = originTile.GetComponent<MapTile>().index;
		tableGO.transform.SetParent(tileContainerGO.transform, true);

		furniture.AnimateIn();
	}

	// Return true if index exists in our current tile set. False if out of bounds.
	private bool TileIndexInRange(Vector2 index) {
		if (index.x >= 0 && index.y >= 0 && index.x < MapSize.x && index.y < MapSize.y) {
			return true;
		}
		return false;
	}

	// Quick way to nicely animate tiles in
	private void AnimateTilesIn() {
		int index = 0;
		for (int y = 0; y < MapSize.y; y++) {
			for (int x = 0; x < MapSize.x; x++) {
				GameObject tile = tiles[x, y];
				Vector2 originalPosition = tile.transform.position;
				tile.transform.position = new Vector2(originalPosition.x, originalPosition.y - 5.0f);
				iTween.MoveTo(tile, iTween.Hash("time", 1.0f, "delay", index * 0.005f + Random.Range(0f, 0.2f), "y", originalPosition.y));
				index++;
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TileSetData", menuName = "Map/TileSetData", order = 1)]

public class TileSetData : ScriptableObject {
	public Sprite[] sprites;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {
	MapManager mapManager;
	// Use this for initialization
	void Awake () {
		GameObject mapManagerGO = GameObject.FindWithTag("MapManager");
		if (!mapManagerGO) {
			Debug.LogError("Could not find gameobject with tag MapManager");
			return;
		}

		mapManager = mapManagerGO.GetComponent<MapManager>();
		if (!mapManager) {
			Debug.LogError("Failed to load MapManager component from gameobject with tag MapManager");
			return;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			GameObject target = SelectedGameObject();
			MapTile mapTile = null;
			if (target == null) {
				Debug.Log("No selection made.");
				return;
			}

			Debug.Log("Selected: " + target.name);
			mapTile = target.GetComponent<MapTile>();
			// Is this a map tile?
			if (mapTile != null) {
				mapManager.PlaceFurnitureIfPossible(mapTile.index);
			}
		}
	}

	GameObject SelectedGameObject() {
		// FIXME: Optimize by only casting ray on specific physics layers if possible
		GameObject target = null;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction * 20, Mathf.Infinity);
		if (hit) {
			Debug.DrawLine(ray.origin, hit.point);
			target = hit.collider.gameObject;
		}
		return target;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour {
	public float destroyAfter = 2.0f;

	// Use this for initialization
	void Start () {
		StartCoroutine(DestroyAfterTime());
	}
	
	IEnumerator DestroyAfterTime() {
		yield return new WaitForSeconds(destroyAfter);
		Destroy(gameObject);
	}
}
